package com.sapient.week1;

public class IntegerArray {
	int[] arr;
	
	public IntegerArray() {
		System.out.println("0 Argument");
		arr = new int[10];
	}
	
	public IntegerArray(int n) {
		System.out.println("1 Argument");
		arr = new int[n];
	}
	
	public IntegerArray(IntegerArray array) {
		System.out.println("copy constructor");
		this.arr = array.arr;
	}
	
	public IntegerArray(int x[]) {
		System.out.println("Array Argument");
		arr = x;
	}
	
	public void read() {
		System.out.println("Enter " + arr.length + " elements ");
		for(int i=0;i<arr.length;i++) {
			arr[i] = Read.sc.nextInt();
		}
	}
	
	public void display() {
		for(int i=0;i<arr.length;i++) {
			System.out.print(arr[i] + "  ");
		}
		System.out.println();
	}
	
	public void display(int size) {
		for(int i=0;i<size;i++) {
			System.out.print(arr[i] + "  ");
		}
		System.out.println();
	}
	
	public float Average() {
		float sum=0f;
		for(int i=0;i<arr.length;i++) {
			sum+=arr[i];
		}
		return sum/arr.length;
	}
	
	public void Search(int k) {
		int ind = -1;
		for(int i=0;i<arr.length;i++) {
			if(arr[i]==k){
				ind = i+1;
				break;
			}
		}
		if(ind==-1) {
			System.out.println("Not Found");
		}
		else {
			System.out.println("Found at position "+ ind);
		}
	}
	
	public void sort() {
		int n = arr.length;
		for(int i=0;i<n;i++) {
			for(int j=0;j<n-1;j++) {
				if(arr[j]>arr[j+1]){
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
	
	}
}